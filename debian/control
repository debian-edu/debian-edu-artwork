Source: debian-edu-artwork
Section: graphics
Priority: optional
Maintainer: Debian Edu Developers <debian-edu@lists.debian.org>
Uploaders: Petter Reinholdtsen <pere@debian.org>,
           Holger Levsen <holger@debian.org>,
           Mike Gabriel <sunweaver@debian.org>,
Build-Depends: advancecomp,
               debhelper-compat (= 13),
               devscripts,
               dh-exec,
               fonts-quicksand,
               imagemagick,
               inkscape,
               librsvg2-bin,
               optipng,
               rdfind,
               symlinks,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://blends.debian.org/edu
Vcs-Browser: https://salsa.debian.org/debian-edu/debian-edu-artwork
Vcs-Git: https://salsa.debian.org/debian-edu/debian-edu-artwork.git

Package: debian-edu-artwork
Architecture: all
Depends: ${misc:Depends},
         debian-edu-artwork-emerald | debian-edu-artwork-homeworld | debian-edu-artwork-buster | debian-edu-artwork-futureprototype | debian-edu-artwork-softwaves | debian-edu-artwork-lines | debian-edu-artwork-joy | debian-edu-artwork-spacefun,
         desktop-base,
         libconfig-inifiles-perl,
Suggests: debian-edu-artwork-spacefun
Enhances: gdm3,
          lightdm,
          sddm,
          slim,
Description: Debian Edu themes and artwork
 This package contains a collection of images, themes, wallpapers and splash
 screens for use with Debian Edu, a Debian Pure Blend.
 .
 This package contains files that all Debian Edu themes have in common.

Package: debian-edu-artwork-softwaves
Architecture: all
Depends: ${misc:Depends},
         desktop-base,
Recommends: debian-edu-artwork,
Description: Debian Edu softWaves ("stretch") theme and artwork
 The Debian Edu Artwork package set provides a collection of images, themes,
 wallpapers and splash screens for use with Debian Edu, a Debian Pure Blend.
 .
 This package contains all files for the Debian Edu Softwaves artwork for the
 "stretch" based release.

Package: debian-edu-artwork-buster
Architecture: all
Depends: ${misc:Depends},
         desktop-base,
Recommends: debian-edu-artwork,
            fonts-quicksand,
Description: Debian Edu futurePrototype "buster" theme and artwork
 The Debian Edu Artwork package set provides a collection of images, themes,
 wallpapers and splash screens for use with Debian Edu, a Debian Pure Blend.
 .
 This package contains all files for the Debian Edu artwork for the "buster"
 based release.

Package: debian-edu-artwork-homeworld
Architecture: all
Depends: ${misc:Depends},
         desktop-base,
Recommends: debian-edu-artwork
Description: Debian Edu Homeworld "bullseye" theme and artwork
 The Debian Edu Artwork package set provides a collection of images, themes,
 wallpapers and splash screens for use with Debian Edu, a Debian Pure Blend.
 .
 This package contains all files for the Debian Edu artwork for the "bullseye"
 based release.

Package: debian-edu-artwork-emerald
Architecture: all
Depends: ${misc:Depends},
         desktop-base,
Recommends: debian-edu-artwork
Description: Debian Edu Emerald "bookworm" theme and artwork
 The Debian Edu Artwork package set provides a collection of images, themes,
 wallpapers and splash screens for use with Debian Edu, a Debian Pure Blend.
 .
 This package contains all files for the Debian Edu artwork for the "bookworm"
 based release.
